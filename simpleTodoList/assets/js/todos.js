$('#todos li').click(function() {
  if ($(this).css('color') === 'rgb(128, 128, 128)') {
    $(this).css({
      color: 'black',
      textDecoration: 'none'
    });
  } else {
    $(this).css({
      color: 'gray',
      textDecoration: 'line-through'
    });
  }

});
$('#semangat').append(
  "<audio controls autoplay hidden> <source src='../css/semangat.mp3' type='audio/mpeg' /></audio>"
);
// lengkapi jquery untuk menlist hasil dari todolist yang di inputkan
$(document).on('click', 'li', function() {
  var conf = confirm("OK untuk hapus, CANCEL untuk mencoret daftar");
  if (conf) {
    $(this).remove();
  }
});



$('#plus').click(function(){
  $('#todos').append(
    "<li><span><i class='fa fa-trash'></i></span>"+$('#new').val()+"</li>"
    );
});
  